<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php
        require_once 'frog.php';
        require_once 'ape.php';

        // sheep
        $sheep = new Animal("shaun");

        echo $sheep->nama;
        echo $sheep->name; 
        echo $sheep->kaki;
        echo $sheep->legs;
        echo $sheep->darah; 
        echo $sheep->cold_blooded . "<br>"; 

        // kodok
        $kodok = new Kodok("Buduk");

        echo $kodok->nama;
        echo $kodok->name; 
        echo $kodok->kaki;
        echo $kodok->legs;
        echo $kodok->darah; 
        echo $kodok->cold_blooded;
        echo $kodok->loncat;
        echo $kodok->jump() . "<br><br>"; 

        // kera
        $kera = new Ape("Kera sakti");

        echo $kera->nama;
        echo $kera->name; 
        echo $kera->kaki;
        echo $kera->legs;
        echo $kera->darah; 
        echo $kera->cold_blooded;
        echo $kera->teriak;
        echo $kera->yell();
    ?>
</body>
</html>
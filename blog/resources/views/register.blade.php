<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form Pendaftaran</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign up form</h3>
    <form action="/welcome" method="POST">
        @csrf
        <label>First name:</label>
        <br><br>
        <input type="text" name="nama depan">
        <br><br>
        <label>Last name:</label>
        <br><br>
        <input type="text" name="nama belakang">
        <br><br>
        <label>Gender:</label>
        <br><br>
        <input type="radio" name="jenis kelamin">Male
        <br>
        <input type="radio" name="jenis kelamin">Female
        <br>
        <input type="radio" name="jenis kelamin">Other
        <br><br>
        <label>Nationaly:</label>
        <br><br>
        <select name="WN">
            <option value="Indonesia">Indonesian</option>
            <option value="Singapura">Singapore</option>
            <option value="Malaysia">Malaysia</option>
        </select>
        <br><br>
        <label>Language Spoken:</label>
        <br><br>
        <input type="checkbox" name="Indo">Bahasa Indonesia
        <BR>
        <input type="checkbox" name="inggris">English
        <br>
        <input type="checkbox" name="lain-lain">Other
        <br><Br>
        <label>Bio:</label>
        <br><br>
        <textarea name="Bio" cols="30" rows="10"></textarea>
        <br>
        <button type="submit" name="sign up">Sign Up</button>
    </form>
</body>
</html>
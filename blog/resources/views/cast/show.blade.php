@extends('master')

@section('content')
    <div class="mx-3 my-2">
        <h2>Show Cast {{$cast->id}}</h2>
        <h4>{{$cast->nama}}</h4>
        <p>{{$cast->umur}} tahun</p>
        <p>{{$cast->bio}}</p>
    </div>
@endsection
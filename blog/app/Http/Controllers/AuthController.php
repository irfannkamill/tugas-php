<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function sapa(Request $request){
        $namaDepan = $request["nama_depan"];
        $namaBelakang = $request["nama_belakang"];
        // echo "<h1>Selamat Datang " . $namaDepan . " " . $namaBelakang . "</h1>";
        return view('welcome', compact('namaDepan', 'namaBelakang'));
    }

    public function form(){
        return view('register');
    }
}
